﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace insort
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void View (Record[] array, DataGridView dgv)
        {
            dgv.RowCount = 1;
            dgv.ColumnCount = array.Length;
            dgv.ClearSelection();
            for (int i = 0; i < array.Length; i++)
            {
                dgv.Rows[0].Cells[i].Value = array[i].Key;
            }
            dgv.Refresh();
            Thread.Sleep(1000);
        }

        private void Sort (Record[] array, int min, int max)
        {
            Record[] arrayI = new Record[max - min + 1];
            for (int i = 0; i < arrayI.Length; i++)
            {
                arrayI[i] = new Record();
                arrayI[i].Key = 0;
                arrayI[i].Ch = ' ';
            }
            View(arrayI, dataGridView2);
            for (int i = 0; i < array.Length; i++)
            {
                dataGridView1.Rows[0].Cells[i].Style.BackColor = Color.Green;
                dataGridView1.Refresh();
                int j = array[i].Key - min;
                arrayI[j].Key++;
                arrayI[j].Ch = array[i].Ch;
                dataGridView2.Rows[0].Cells[j].Style.BackColor = Color.Green;
                View(arrayI, dataGridView2);
                dataGridView1.Rows[0].Cells[i].Style.BackColor = Color.White;
                dataGridView2.Rows[0].Cells[j].Style.BackColor = Color.White;
            }
            View(arrayI, dataGridView2);
            int k = 0;
            for (int i = 0; i < arrayI.Length; i++)
            {
                dataGridView2.Rows[0].Cells[i].Style.BackColor = Color.Green;
                dataGridView2.Refresh();
                for (int j = 0; j < arrayI[i].Key; j++, k++)
                {
                    dataGridView1.Rows[0].Cells[k].Style.BackColor = Color.Green;
                    array[k].Key = i + min;
                    array[k].Ch = arrayI[i].Ch;
                    View(array, dataGridView1);
                    dataGridView1.Rows[0].Cells[k].Style.BackColor = Color.White;
                }
                dataGridView2.Rows[0].Cells[i].Style.BackColor = Color.White;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Random random = new Random();
            int size = (int)numericUpDown1.Value;
            int min = (int)numericUpDown2.Value;
            int max = (int)numericUpDown3.Value;
            Record[] array = new Record[size];
            for (int i = 0; i < size; i++)
            {
                array[i] = new Record();
                array[i].Key = random.Next(min, max);
                array[i].Ch = Convert.ToChar(i);
            }
            View(array, dataGridView1);
            Sort(array, min, max);
            MessageBox.Show("done!");
        }
    }
}
